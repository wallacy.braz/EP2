OBJETIVO

Este projeto faz parte da avaliação da disciplina de orientação à objetos, ministrado pelo Prof. Paulo Meireles, Pela UnB, faculdade do Gama.

DESCRIÇÂO

Este projeto tem a finalidade de simular um sistema de um restaurante. Para tanto ele conta com 3 operações possíveis: Controle de estoque de produtos, registro de pedidos e execução de pagamentos.


REQUISITOS

Para fazer a instalação deste projeto os seguintes itens são requeridos instalados na máquina que se deseja rodar o software:
-NetBeans IDE
-Java Virtual Machine


INSTALAÇÂO

Para fazer a instalação deste projeto, basta copiar o projeto para a pasta desejada e depois abrir o projeto com o aplicativo NetBeans IDE.

UTILIZAÇÂO

Assim que o software estiver, basta seguir as opções desejadas.
