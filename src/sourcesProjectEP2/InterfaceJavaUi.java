/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sourcesProjectEP2;
import java.util.InputMismatchException;
import java.util.Scanner;


/**
 *
 * @author wallacy
 */
public class InterfaceJavaUi {
    
    
    
    static Estoque estoque = new Estoque();
    static ControlePedidos controle = new ControlePedidos();
    
    public static void main(String[] args) {
    
    Scanner in = new Scanner(System.in);
    boolean teste = true;
    int op1 = 5;
    
    
    while (true) {
        System.out.println("Digite a opcão desejada: ");
        System.out.println("0- Cadastrar Produto");
        System.out.println("1- Alterar produto");
        System.out.println("2- Fazer pedido");
        System.out.println("3- Ver relatório");
        System.out.println("4- Sair");
        op1 = in.nextInt();   
        
            
        
        
    
        switch(op1) {
            case 0 : {
            
                Produto novoProduto = new Produto();
                System.out.println("Digite o nome: ");
                novoProduto.setNome(in.next());
                System.out.println("Digite o preço: ");
                novoProduto.setPrice(in.nextDouble());
                System.out.println("Digite a quantidade inicial:");
                novoProduto.setQtde(in.nextInt());
                estoque.addProduto(novoProduto);
                break;
            }
        
            case 1: {
                Produto altera;
                System.out.println("Digite o id do produto que queira alterar :");
                int idTemp = in.nextInt();
                altera = estoque.getProdutos().get(idTemp);
                System.out.println("Digite o campo do produto que deseje alterar:");
                System.out.println("0- Preço");
                System.out.println("1- Quantidade");
                op1 = in.nextInt();
                switch(op1) {
                    case 0 : {
                        System.out.println("Digite o novo preço:");
                        altera.setPrice(in.nextDouble());
                        System.out.println("Preço alterado");
                        break;
                    }
                    case 1: {
                        System.out.println("Digite a nova quantidade: "); 
                        altera.setQtde(in.nextInt());
                        System.out.println("Preço alterado!");
                        break;
                    }
                
                }
                break; 
            
            }
            case 2: {
                Pedido novo = new Pedido();
                int n = 0;
                String nome;
                String cpf;
                String coment = "";
                double temp= 0;
                boolean confirm=true;
            
                System.out.println("Bem vindo ao To com fome! Quero mais!");
                System.out.println("Digite o nome e o cpf do cliente");
                nome = in.next();
                cpf = in.next();
                Cliente novoCliente = new Cliente(nome,cpf);
                novo.setDono(novoCliente);
                while(confirm) {
                    System.out.println("Escolha o item que quer adicionar ao pedido");
                    for(Produto i: estoque.getProdutos()) {
                    System.out.print(n);
                    System.out.print(" "+i.getNome());
                    System.out.println(" "+i.getPrice());
                    n++;
                    }
                    n = in.nextInt();
                    Produto ref;
                    ref = estoque.getProdutos().get(n);
                    novo.getProdutosPedido().add(ref);
                    temp +=ref.getPrice();
                    novo.setPriceTotal(temp);
                    
                    
                    
                
                    System.out.println("Deseja adicionar mais itens?");
                    System.out.println("sim/não");
                    String resposta;
                    resposta = in.next();
                    if("sim".equals(resposta)) {
                        confirm =  true;
                    } 
                    else if ("não".equals(resposta)) {
                        confirm = false;
                    }
                    
                    
                    
                
                }
                
                System.out.println("Qual a forma de pagamento?");
                System.out.print("0-CartaoPrazo\n1-CartaoVista\n");
                n = in.nextInt(); 
                switch(n) {
                    case 0: { 
                        PagamentoPrazo pagamento;
                        pagamento = new PagamentoPrazo();
                        pagamento.setPedidoRelacionado(novo);
                        pagamento.setValorPagamento(pagamento.getPedidoRelacionado().getPriceTotal());
   
                        System.out.println("Deseja dividir o valor?");
                        System.out.println("sim/não");
                        if("sim".equals(in.next())) {
                            System.out.print("Quantas vezes?");
                            System.out.print("1x sem juros\n2x sem juros\n3x sem juros\n4x sem juros\n5x sem juros");
                            n = in.nextInt();
                            pagamento.setVezesParcelamento(n);
                        }
                        else if("não".equals(in.next())) {
                            pagamento.setVezesParcelamento(1);
                        }
                        pagamento.realizarPagamento();
                        break;
                    }
                    case 1:
                    {
                        PagamentoVista pagamento;
                        pagamento = new PagamentoVista();
                        pagamento.setPedidoRelacionado(novo);
                        pagamento.setValorPagamento(pagamento.getPedidoRelacionado().getPriceTotal());
                        pagamento.realizarPagamento();
                    }
                        
                }
                for(Produto i: novo.getProdutosPedido()) {
                 n = i.getQtde();
                 n--;
                 i.setQtde(n);                                   
                }
                    
                    
                
            controle.addPedido(novo);
            break;   
            }
            case 3: {
                controle.printarRelatorio();
                break;
            }
            case 4: {
                System.exit(0);
            }
        }
        
       }
        
    }
}  

