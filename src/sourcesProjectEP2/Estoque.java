/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sourcesProjectEP2;

import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author wallacy
 */
public class Estoque {
    private ArrayList <Produto> produtos ;
    
    public Estoque() {
        produtos = new ArrayList<>();
    }
    

    
    
    public void addProduto(Produto produto) {
        getProdutos().add(produto);
    }
    
    public void remProduto(Produto produto) {
        getProdutos().remove(getProdutos().indexOf(produto));
    }
    
    public void addQtde(Produto produto, int qtde) {
        int temp  = produto.getQtde();
        int newQtde = temp + qtde;
        produto.setQtde(newQtde);
    }
    
    public void remoeQtde(Produto produto, int qtde) {
        if(produto.getQtde()>=0) {
            int temp = produto.getQtde();
            int newQtde = produto.getQtde() - qtde;
            produto.setQtde(newQtde);
        }
    }

    /**
     * @return the produtos
     */
    public ArrayList <Produto> getProdutos() {
        return produtos;
    }

    /**
     * @param produtos the produtos to set
     */
    public void setProdutos(ArrayList <Produto> produtos) {
        this.produtos = produtos;
    }
    
    
}
