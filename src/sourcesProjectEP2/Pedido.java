/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sourcesProjectEP2;

import java.util.ArrayList;

/**
 *
 * @author wallacy
 */
public class Pedido {
    private static int idPedidoGeral = 0;
    private int idPedido;
    private double priceTotal;
    private ArrayList <Produto> produtosPedido;
    private Cliente dono;
    

    
    
    public Pedido () {
       this.idPedido = Pedido.idPedidoGeral;
       Pedido.idPedidoGeral++;
       produtosPedido = new ArrayList<>();
    }
    
    /**
     * @return the idPedido
     */
    public int getIdPedido() {
        return idPedido;
    }

    /**
     * @param idPedido the idPedido to set
     */
    public void setIdPedido(int idPedido) {
        this.idPedido = idPedido;
    }

    /**
     * @return the priceTotal
     */
    public double getPriceTotal() {
        return priceTotal;
    }

    /**
     * @param priceTotal the priceTotal to set
     */
    public void setPriceTotal(double priceTotal) {
        this.priceTotal = priceTotal;
    }

    /**
     * @return the produtosPedido
     */
    public ArrayList <Produto> getProdutosPedido() {
        return produtosPedido;
    }

    /**
     * @param produtosPedido the produtosPedido to set
     */
    public void setProdutosPedido(ArrayList <Produto> produtosPedido) {
        this.produtosPedido = produtosPedido;
    }

    /**
     * @return the dono
     */
    public Cliente getDono() {
        return dono;
    }

    /**
     * @param dono the dono to set
     */
    public void setDono(Cliente dono) {
        this.dono = dono;
    }
    
    
    
    
    
}
