/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sourcesProjectEP2;

import java.util.ArrayList;

/**
 *
 * @author wallacy
 */
public class ControlePedidos {
    
    private ArrayList<Pedido> controlePedido;
    
    public ControlePedidos() {
        controlePedido = new ArrayList<>();
    }
    
    
    /**
     * @return the controlePedido
     */
    public ArrayList<Pedido> getControlePedido() {
        return controlePedido;
    }

    /**
     * @param controlePedido the controlePedido to set
     */
    public void setControlePedido(ArrayList<Pedido> controlePedido) {
        this.controlePedido = controlePedido;
    }
    
    public void removerPedido(int idPedido) {
        Pedido remover = controlePedido.get(idPedido);
        controlePedido.remove(remover);
    }
    
    public void addPedido(Pedido pedido) {
        controlePedido.add(pedido);
    }
    
    public void printarRelatorio() {
        String temp;
        for(Pedido i: controlePedido) {
            temp = String.format("Pedido : %d\tCliente: %s\tValor:%.2f",i.getIdPedido(),i.getDono().getNome(),i.getPriceTotal());
            System.out.println(temp+"\n");
        }
    }
    
    
    
}
