/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sourcesProjectEP2;

/**
 *
 * @author wallacy
 */
public class Cliente extends Pessoa{
    static private int idGeral =0;
    private int idCliente;


    
    public Cliente (String nome,String cpf) {
        this.idCliente = idGeral;
        idGeral++;
        super.setCpf(cpf);
        super.setNome(nome);
    }
    
    
    
    
    /**
     * @return the idCliente
     */
    
    
    
    
    public int getIdCliente() {
        return idCliente;
    }

    /**
     * @param idCliente the idCliente to set
     */
    public void setIdCliente(int idCliente) {
        this.idCliente = idCliente;
    }

    
    
    
    
    
}
