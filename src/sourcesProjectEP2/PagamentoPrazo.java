/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sourcesProjectEP2;

/**
 *
 * @author wallacy
 */
public class PagamentoPrazo extends Pagamento{
    
    private int vezesParcelamento;

    /**
     * @return the vezesParcelamento
     */
    public int getVezesParcelamento() {
        return vezesParcelamento;
    }

    /**
     * @param vezesParcelamento the vezesParcelamento to set
     */
    public void setVezesParcelamento(int vezesParcelamento) {
        this.vezesParcelamento = vezesParcelamento;
    }
    
    
    @Override
    public void realizarPagamento() {
        System.out.println("Pagamento efetuado!");
        System.out.println("Valor total: "+super.getValorPagamento());
        System.out.println("Em "+this.getVezesParcelamento()+" vezes de "+String.format("%.2f",(this.getValorPagamento()/getVezesParcelamento())));    
    }
    
    
}


