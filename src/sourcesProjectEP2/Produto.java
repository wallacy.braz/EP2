/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sourcesProjectEP2;

/**
 *
 * @author wallacy
 */
public class Produto {
    
    static private int id =0; 
    private int idProduto;
    private String nome;
    private double price;
    private int qtde;
    
    Produto() {
        this.idProduto = id;
        Produto.id++;
    }
    
    Produto(String nome, double price, int qtde) {
       this.idProduto = Produto.id;
       this.nome = nome;
       this.price = price;
       this.qtde = qtde;
       Produto.id++;
    }
    
    /**
     * @return the id
     */
    public static int getId() {
        return id;
    }

    /**
     * @param aId the id to set
     */
   
    

    /**
     * @return the idProduto
     */
    public int getIdProduto() {
        return idProduto;
    }

    /**
     * @param idProduto the idProduto to set
     */
    public void setIdProduto(int idProduto) {
        this.idProduto = idProduto;
    }

    /**
     * @return the nome
     */
    public String getNome() {
        return nome;
    }

    /**
     * @param nome the nome to set
     */
    public void setNome(String nome) {
        this.nome = nome;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) {
        this.price = price;
    }

    /**
     * @return the qtde
     */
    public int getQtde() {
        return qtde;
    }

    /**
     * @param qtde the qtde to set
     */
    public void setQtde(int qtde) {
        this.qtde = qtde;
    }
    
    
    
}
